Sheep for CSS3
----------------------------------------------------------------------------
Sheep for CSS3 is an implementation [sheep] for CSS3 Animation.

Currently, sheep for CSS3 supports only Webkit Browser like Chrome.
If you use the other browser, it may be not work.

This platform concept is to work with only pure css and javascripts, without any libraries.
If it supports any platforms with the library, it should be [sheep-jqm] project.


Demonstration

http://woolmark.bitbucket.org/css3/


License
----------------------------------------------------------------------------
[wtfpl] 

[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[sheep-jqm]: https://bitbucket.org/woolmark/sheep-jqm "Sheep for JQM"
[wtfpl]: http://www.wtfpl.net "WTFPL"